import 'package:flutter/material.dart';
import 'package:simple_rest_api_dio/api/api_client.dart';
import 'package:simple_rest_api_dio/api/endpoints.dart';
import 'package:simple_rest_api_dio/models/user_model.dart';

class HomePage extends StatelessWidget {
  var apiClient = new ApiClient();

  Future<List<UserModel>> _fecthDataUsers() async {
    var response = await apiClient.get(user_endpoint);
    var users = (response.data['data'] as List)
        .map((e) => UserModel.fromJson(e))
        .toList();
    return users;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Belajar GET HTTP'),
      ),
      body: Container(
        child: FutureBuilder<List<UserModel>>(
          future: _fecthDataUsers(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  padding: EdgeInsets.all(10),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    final user = snapshot.data![index];
                    print(user);

                    return ListTile(
                      leading: CircleAvatar(
                        radius: 30,
                        backgroundImage:
                        NetworkImage(user.avatar),
                      ),
                      title: Text(user.firstName + " " + user.lastName),
                      subtitle: Text(user.email),
                    );
                  });
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}