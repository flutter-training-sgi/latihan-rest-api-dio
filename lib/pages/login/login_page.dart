import 'package:flutter/material.dart';
import 'package:simple_rest_api_dio/api/api_client.dart';
import 'package:simple_rest_api_dio/api/endpoints.dart';
import 'package:simple_rest_api_dio/pages/home/home_page.dart';
import 'dart:convert';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  String email = "";
  String password = "";
  String sessionToken = "";
  final passTextController = TextEditingController();
  var apiClient = new ApiClient();

  @override
  void initState() {
    super.initState();

    // Start listening to changes.
    passTextController.addListener(_printLatestValue);
  }

  void _printLatestValue() {
    print('Second text field: ${passTextController.text}');
    password = passTextController.text;
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    passTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    Future<bool> PostLogin(String email, String password) async {
      var loginData = {'email': email, 'password': password};
      var response = await apiClient.post(login_endpoint, data: loginData);
      //print(response);
      try {
        var loginRespJson = response.data;
        if (loginRespJson.containsKey('token')) {
          sessionToken = loginRespJson['token'];
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => HomePage()));
          return true;
        } else {
          print("Login gagal");
          return false;
        }
      } catch(e) {
        print("Error");
        print(e.toString());
        return false;
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "E-mail",
                      hintText: "eve.holt@reqres.in cityslicka",
                      icon: Icon(Icons.email),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'E-mail tidak boleh kosong';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        email = value;
                      });
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(
                      labelText: "Password",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Password tidak boleh kosong';
                      }
                      return null;
                    },
                    controller: passTextController,
                  ),
                ),
                ElevatedButton(
                  child: Text(
                    "LOGIN",
                    style: TextStyle(color: Colors.white),
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor:Colors.blue
                  ),
                  onPressed: () {
                    //if (_formKey.currentState!.validate()) {
                      print("Login button pressed");
                      PostLogin(email, password);
                    //}
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}